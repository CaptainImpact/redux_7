import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux7/elements/language/language_selector.dart';
import 'package:redux7/elements/theme/theme_selector.dart';
import 'package:redux7/store/app/app_state.dart';

class MainPageViewModel {
  final void Function(ThemeData themeData) changeTheme;
  final ThemeData currentTheme;
  final void Function(String locale) changeLanguage;
  final String locale;

  MainPageViewModel({
    @required this.changeTheme,
    @required this.currentTheme,
    @required this.changeLanguage,
    @required this.locale,
  });

  static MainPageViewModel fromStore(Store<AppState> store) {
    return MainPageViewModel(
      changeTheme: ThemeSelector.changeTheme(store),
      currentTheme: ThemeSelector.getCurrentTheme(store),
      changeLanguage: LanguageSelector.changeLanguage(store),
      locale: LanguageSelector.getSelectedLocale(store),
    );
  }
}

import 'package:redux7/elements/language/language_state.dart';
import 'package:redux7/elements/theme/theme_state.dart';

class AppState {
  final LanguageState languageState;
  final ThemeState themeState;

  AppState({this.languageState, this.themeState});

  factory AppState.initial() {
    return AppState(
      languageState: LanguageState.initial(),
      themeState: ThemeState.initial(),
    );
  }

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';

    print('$TAG => <appReducer> => action: ${action.runtimeType}');

    return AppState(
      themeState: state.themeState.reducer(action),
      languageState: state.languageState.reducer(action),
    );
  }
}

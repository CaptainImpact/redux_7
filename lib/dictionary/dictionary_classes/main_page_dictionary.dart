import 'package:flutter/material.dart';

class MainScreenDictionary {
  final String notFound;
  final String clear;
  final String watchRecipes;
  final String chooseFood;
  final String delete;

  MainScreenDictionary({
    @required this.notFound,
    @required this.clear,
    @required this.watchRecipes,
    @required this.chooseFood,
    @required this.delete,
  });
}
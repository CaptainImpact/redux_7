import 'package:flutter/foundation.dart';

class NavigationDictionary {
  final String home;
  final String back;
  final String favorites;
  final String settings;
  final String welcomeLogin;
  final String registerGoogle;

  NavigationDictionary({
    @required this.home,
    @required this.favorites,
    @required this.settings,
    @required this.back,
    @required this.welcomeLogin,
    @required this.registerGoogle,
  });
}

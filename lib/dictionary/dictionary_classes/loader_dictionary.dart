import 'package:flutter/foundation.dart';

class LoaderDictionary {
  final String loader;

  LoaderDictionary({@required this.loader});
}

import 'package:flutter/foundation.dart';

class SettingsScreenDictionary {
  final String settings;
  final String logOut;
  final String language;
  final String notification;
  final String aboutCompany;


  SettingsScreenDictionary({
    @required this.settings,
    @required this.logOut,
    @required this.language,
    @required this.notification,
    @required this.aboutCompany,

  });
}

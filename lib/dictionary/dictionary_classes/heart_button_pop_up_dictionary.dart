import 'package:flutter/foundation.dart';

class HeartButtonPopUpDictionary {
  final String addPopUpText;
  final String removePopUpText;


  HeartButtonPopUpDictionary({
    @required this.addPopUpText,
    @required this.removePopUpText
  });
}

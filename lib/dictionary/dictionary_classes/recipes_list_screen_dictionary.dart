import 'package:flutter/foundation.dart';

class RecipesListScreenDictionary {
  final String foodElements;
  final String cooking;
  final String similarRecipes;
  final String recipes;
  final String dontHave;
  final String backText;

  RecipesListScreenDictionary({
    @required this.dontHave,
    @required this.foodElements,
    @required this.cooking,
    @required this.similarRecipes,
    @required this.recipes,
    @required this.backText,
  });
}

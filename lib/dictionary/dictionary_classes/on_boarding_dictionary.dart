import 'package:flutter/foundation.dart';

class OnBoardingDictionary {
  final String chooseProducts;
  final String pickGroceries;
  final String selectRecipe;
  final String followRecipe;
  final String next;
  final String start;

  OnBoardingDictionary({
    @required this.start,
    @required this.next,
    @required this.chooseProducts,
    @required this.pickGroceries,
    @required this.selectRecipe,
    @required this.followRecipe,
  });
}

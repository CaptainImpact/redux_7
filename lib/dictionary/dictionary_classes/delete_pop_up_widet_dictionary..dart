import 'package:flutter/foundation.dart';

class DeletePopUpWidgetDictionary {
  final String deletePopUpText;
  final String confirm;
  final String cancel;

  DeletePopUpWidgetDictionary({
    @required this.deletePopUpText,
    @required this.confirm,
    @required this.cancel,
  });
}

import 'package:flutter/material.dart';

class ChangeThemeAction {
  final ThemeData themeData;

  ChangeThemeAction(this.themeData);
}
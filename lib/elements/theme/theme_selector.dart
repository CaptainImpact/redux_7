import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux7/elements/theme/theme_actions.dart';
import 'package:redux7/store/app/app_state.dart';

class ThemeSelector{

  static ThemeData getCurrentTheme(Store<AppState> store){
    return store.state.themeState.themeData;
  }
  static void Function(ThemeData themeData) changeTheme(Store<AppState> store){
    return (ThemeData themeData) => store.dispatch(ChangeThemeAction(themeData));
  }
}
import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:redux7/elements/theme/theme_actions.dart';
import 'package:redux7/store/app/reducer.dart';

class ThemeState {
  final ThemeData themeData;

  ThemeState({
    @required this.themeData,
  });

  factory ThemeState.initial() {
    return ThemeState(
      themeData: ThemeData(
        brightness: Brightness.light,
      ),
    );
  }

  ThemeState copyWith({@required ThemeData themeData}) {
    return ThemeState(
      themeData: themeData ?? this.themeData,
    );
  }

  ThemeState reducer(dynamic action) {
    return Reducer<ThemeState>(
      actions: HashMap.from({
        ChangeThemeAction: (dynamic action) => changeThemeAction(action as ChangeThemeAction),
      }),
    ).updateState(action, this);
  }

  ThemeState changeThemeAction(ChangeThemeAction action) {
    return this.copyWith(
      themeData: action.themeData,
    );
  }
}



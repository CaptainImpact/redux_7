import 'package:redux/redux.dart';
import 'package:redux7/elements/language/language_actions.dart';

import 'package:redux7/store/app/app_state.dart';

class LanguageSelector {
  static String getSelectedLocale(Store<AppState> store) {
    return store.state.languageState.locale;
  }

  static void Function(String) changeLanguage(Store<AppState> store) {
    return (String locale) => store.dispatch(ChangeLanguageAction(locale: locale));
  }
}


import 'package:flutter/material.dart';
import 'package:redux7/elements/base_action.dart';


class ChangeLanguageAction extends BaseAction {
  final String locale;

  ChangeLanguageAction({@required this.locale}) : super(type: 'ChangeLanguageAction');
}

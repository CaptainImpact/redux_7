import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux7/dictionary/flutter_delegate.dart';
import 'package:redux7/dictionary/flutter_dictionary.dart';
import 'package:redux7/store/app/app_state.dart';

import 'main_page_viewmodel.dart';

void main() {
  Store store = Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
  );
  runApp(
    MyApp(
      store: store,
    ),
  );
}

class MyApp extends StatelessWidget {
  final Store<AppState> store;

  const MyApp({Key key, this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
      store: store,
      child: StoreConnector<AppState, MainPageViewModel>(
        converter: MainPageViewModel.fromStore,
        builder: (BuildContext context, MainPageViewModel viewModel) {
          return MaterialApp(
            localizationsDelegates: [
              ...FlutterDictionaryDelegate.getLocalizationDelegates,
            ],
            locale: Locale(viewModel.locale),
            theme: viewModel.currentTheme,
            home: MyHomePage(title: FlutterDictionary.instance.dictionary?.test ?? 'Test'),
          );
        },
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    FlutterDictionary.instance.setNewLanguage(FlutterDictionaryDelegate.getCurrentLocale);
    return StoreConnector<AppState, MainPageViewModel>(
      converter: MainPageViewModel.fromStore,
      builder: (BuildContext context, MainPageViewModel viewModel) {
        return Scaffold(
          appBar: AppBar(
            title: Text(
              widget.title,
            ),
          ),
          drawer: Drawer(
            child: Column(
              children: [
                const SizedBox(height: 50.0,),
                Row(
                  children: [
                    RaisedButton(
                      onPressed: () => viewModel.changeLanguage('ru'),
                      child: Text('ru'),
                    ),
                    RaisedButton(
                      onPressed: () => viewModel.changeLanguage('en'),
                      child: Text('en'),
                    ),
                  ],
                ),
                Row(
                  children: [
                    RaisedButton(
                      onPressed: () => viewModel.changeTheme(ThemeData(brightness: Brightness.dark)),
                      child: Text(FlutterDictionary.instance.dictionary.dark),
                    ),
                    RaisedButton(
                      onPressed: () => viewModel.changeTheme(ThemeData(brightness: Brightness.light)),
                      child: Text(FlutterDictionary.instance.dictionary.light),
                    ),
                  ],
                ),
              ],
            ),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[],
            ),
          ),

        );
      },
    );
  }
}
